serg_comment:
	docker-compose run users_api  alembic revision --autogenerate -m "New Migration" 
serg_migrate:
	docker-compose run users_api alembic upgrade head

serg_error:
	docker-compose run users_api alembic stamp head

serg_merge:
	docker-compose run users_api alembic merge heads
