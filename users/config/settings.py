from pydantic import BaseSettings
import databases
import sqlalchemy

class Settings(BaseSettings):

    """
    Хранит в себе данные настроек с валидацией 
    Pydantic
    """
    postgres_db:str = None
    postgres_user:str = None
    postgres_password:str = None
    secret_key:str = '8666vbvhbnb00099'
    email_password:str = None
    

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"
        env_prefix = "USERS_"
        



settings: Settings = Settings()


URL_DATA_BASE = (
                'postgresql://'
                f'{settings.postgres_user}:'
                f'{settings.postgres_password}'
                '@data_users:5432/'
                f'{settings.postgres_db}'
                )

database = databases.Database(URL_DATA_BASE)
engine = sqlalchemy.create_engine(URL_DATA_BASE)
metadata = sqlalchemy.MetaData()