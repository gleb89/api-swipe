from fastapi import FastAPI 
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware

from config import settings
from scr import roters
tags_metadata = [{
    "name": "api",
    "description": "Api сервиса  users",
}]


app = FastAPI(
    openapi_url="/api/v1/users/openapi.json",
    docs_url="/api/v1/users/docs",
)

# app.add_middleware(HTTPSRedirectMiddleware)


# cors настройки!
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)




settings.metadata.create_all(settings.engine)

app.state.database = settings.database


@app.on_event("startup")
async def startup() -> None:
    database_ = app.state.database
    if not database_.is_connected:
        await database_.connect()


@app.on_event("shutdown")
async def shutdown() -> None:
    database_ = app.state.database
    if database_.is_connected:
        await database_.disconnect()

app.mount("/api/v1/users/static", StaticFiles(directory="static"), name="static")


app.include_router(
    roters.app
)

