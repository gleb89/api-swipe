import binascii
import hashlib
import os
from typing import Optional
from datetime import timedelta
from datetime import datetime
from jose import jwt, JWTError

from fastapi.security import OAuth2PasswordBearer
from fastapi.security import OAuth2PasswordBearer
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from fastapi import Request

from PIL import Image 
from PIL import ImageFile




from config.settings import settings




def hash_password(password: str) -> str:
    """Hash a password for storing."""
    salt = b'__hash__' + hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
    pwdhash = hashlib.pbkdf2_hmac('sha512', password.encode('utf-8'),
                                  salt, 100000)
    pwdhash = binascii.hexlify(pwdhash)
    return (salt + pwdhash).decode('ascii')


def is_hash(pw: str) -> bool:
    return pw.startswith('__hash__') and len(pw) == 200


def verify_password(stored_password: str, provided_password: str) -> bool:
    """Verify a stored password against one provided by user"""
    salt = stored_password[:72]
    stored_password = stored_password[72:]
    pwdhash = hashlib.pbkdf2_hmac('sha512',
                                  provided_password.encode('utf-8'),
                                  salt.encode('ascii'),
                                  100000)
    
    pwdhash = binascii.hexlify(pwdhash).decode('ascii')
    return pwdhash == stored_password




# host = 'https://giftcity.kz:8080'
host = 'https://giftcity.kz'


ImageFile.LOAD_TRUNCATED_IMAGES = True
# host = 'https://giftcity.kz:8080'
host = 'http://localhost'


async def image_add(image):
    data = str(datetime.now()).replace(" ", "")
    image = Image.open(image.file)
    resized_im = image.resize((430, 430))
    resized_im.save(f'static/images/{data}.webp', format = "WebP", quality=50,optimize=True)
    origin = f'{host}/api/v1/pets/static/images/{data}.webp'
    return origin


async def image_delete(image_name):

    name_image = image_name.split('images/')[-1]
    path_dir = 'static/images'
    os.remove(path_dir + '/' + name_image)
    return True





ACCESS_TOKEN_EXPIRE_MINUTES = 30
ALGORITHM = "HS256"
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

class AdminToken():

    """
    класс для создания и верефикации пользователя
    по JWT токену
    """

    async def create_access_token(
        self,
        password_hash: dict,
        expires_delta: Optional[timedelta] = None
    ):

        """
        Создает JWT токен из id пользователя
        и назначает его время жизни
        """
        
        to_encode = password_hash.copy()
        if expires_delta:
            expire = datetime.utcnow() + expires_delta
        else:
            expire = datetime.utcnow() + timedelta(minutes=60)
        to_encode.update(
            {"exp": expire}
        )
        encoded_jwt = jwt.encode(
            to_encode,
            settings.secret_key,
            algorithm=ALGORITHM)
        return encoded_jwt

    async def token_password(self, password_hash):

        """
        Принимает id пользователя 
        и создает его hash JWT 
        """

        access_token_expires = timedelta(
            minutes=ACCESS_TOKEN_EXPIRE_MINUTES
        )
        access_token = await self.create_access_token(
            {"sub": str(password_hash)},
            expires_delta=access_token_expires
        )
        return access_token, access_token_expires

    

class JWTBearer(HTTPBearer):

    """
    Класс верефикации действительности токена по
    id_google пользователя
    """

    def __init__(
        self,
        auto_error: bool = True
        ):
        super(
            JWTBearer,
            self).__init__(
            auto_error=auto_error
            )

    async def __call__(

        self, request: Request
        ):

        """
        Проверяет есть ли такой токен в базе токенов
        """

        credentials: HTTPAuthorizationCredentials = await super(
                            JWTBearer, self
                            ).__call__(
                                request
                                )
        
        if credentials:
            return await self.get_current_user(credentials.credentials)
   

    async def get_current_user(
        self,
        token: str 
        ):

        """
        Принимает токен и конвертирует его в данные пользователя
        """
        
        try:
            payload = jwt.decode(
            token,
            settings.secret_key,
            algorithms=[ALGORITHM])
            password = payload.get(
            "sub"
            )
            return password 
        
        except JWTError:
            return JWTError


jwt_auth = JWTBearer()
admin_token = AdminToken()
