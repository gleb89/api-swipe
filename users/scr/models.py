import datetime

import ormar


from pydantic import validator


from config.settings import metadata, database
from .logics import hash_password,is_hash






class Profile(ormar.Model):

    """ 
    """

    class Meta:
        tablename = "profile"
        metadata = metadata
        database = database

    id: int = ormar.Integer(primary_key=True)
    avatar: str = ormar.String(max_length=1000,nullable=True,default='')
    email:str = ormar.String(max_length=1000)
    password:str = ormar.String(max_length=1000,nullable=True, null=True)
    name: str = ormar.String(max_length=1000,nullable=True,default='')
    timestamp_create: datetime.datetime = ormar.DateTime(
                default=datetime.datetime.now()
    ) 

    @validator('password')
    def hash_password(cls, pw: str) -> str:
        if is_hash(pw):
            return pw
        return hash_password(pw)

    class Config:
        orm_mode = True
