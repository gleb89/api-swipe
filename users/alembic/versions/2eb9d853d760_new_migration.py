"""New Migration

Revision ID: 2eb9d853d760
Revises: e1591d071766
Create Date: 2021-12-15 14:54:29.877824

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2eb9d853d760'
down_revision = 'e1591d071766'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('order', 'name')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('order', sa.Column('name', sa.VARCHAR(length=1000), autoincrement=False, nullable=True))
    # ### end Alembic commands ###
